package pooLista;


import java.util.Arrays;
import java.util.Random;
import java.util.Scanner;

public class Lista01 {

	static Scanner sc = new Scanner(System.in);

	public static void main(String[] args) {
		// questao1();
		// questao2();
		// questao3();
		// questao4();
		// questao5();
		// questao6();
		// questao7();
		// questao8();
		// questao9();
		// questao10();
		// questao11();
		// questao12();
		// questao13();
		// questao14();
		// questao15();
		// questao16();
		//questao17();
		// questao18();
		// questao19();
		questao20();
	}

	public static void questao1() {

		System.out.println("Informe o grau em celsius: ");
		double c = sc.nextDouble();

		double f = c * 1.8 + 32;

		System.out.println("O grau celsius " + c + " em Fahrenheit �: " + f);
	}

	public static void questao2() {
		double altura, peso, imc;

		System.out.println("Informe sua altura em metros: ");
		altura = sc.nextDouble();

		System.out.println("Informe seu peso em kgs: ");
		peso = sc.nextDouble();

		imc = peso / (altura * altura);

		if (imc < 18.5) {
			System.out.println("Abaixo do peso." + imc);
		} else if (imc >= 18.5 && imc < 25) {
			System.out.println("Peso normal." + imc);
		} else if (imc >= 25 && imc < 30) {
			System.out.println("Acima do peso." + imc);
		} else {
			System.out.println("Obeso." + imc);
		}

	}

	public static void questao3() {

		double n1, n2, r;
		char x;

		System.out.println("Informe o Numero 1: ");
		n1 = sc.nextDouble();

		System.out.println("Informe o Numero 2: ");
		n2 = sc.nextDouble();
		sc.nextLine();

		System.out.println("Informe o operador (+, -, *, /): ");
		x = sc.nextLine().charAt(0);

		switch (x) {
		case '+':
			System.out.println("Opera��o: " + (n1 + n2));
			break;

		case '-':
			System.out.println("Opera��o: " + (n1 - n2));
			break;

		case '*':
			System.out.println("Opera��o: " + (n1 * n2));
			break;

		case '/':
			System.out.println("Opera��o: " + (n1 / n2));

			break;
		}
	}

	public static void questao4() {
		double pop, taxa = 1.02;
		double prev = 0;
		System.out.println("Informe a popula��o atual: ");
		pop = sc.nextDouble();

		for (int i = 0; i < 5; i++) {

			pop = pop * taxa;
			System.out.printf(
					"Houve um crescimento de: " + (pop - 1000) + " no ano " + (i + 1) + ", totalizando: " + pop);
		}
	}

	public static void questao5() {
		int[] array = new int[10];
		int x = 0;
		for (int i = 0; i < array.length; i++) {
			System.out.println("informe o " + (i + 1) + " valor: ");
			array[i] = sc.nextInt();

		}

		for (int i = 0; i < array.length; i++) {
			for (int j = 0; j < array.length; j++) {

				x = array[i] * array[j];

				if (x % 2 == 0) {
					System.out.println(
							"O elemento na posi��o " + (i) + " vezes o elemento na posi��o " + (j) + " � par " + x);
				}
			}

		}

	}

	public static void questao6() {
		String value, reverseValue = "";

		System.out.println("Informe: ");
		value = sc.nextLine();

		for (int i = value.length() - 1; i >= 0; i--) {
			reverseValue += value.substring(i, i + 1);
		}

		System.out.println(reverseValue);

	}

	public static void questao7() {
		int[] a = new int[10];
		int[] b = new int[10];
		int[] r = new int[10];
		for (int i = 0; i < a.length; i++) {
			System.out.println("Informe o elemento n�mero: " + (i + 1) + " do vetor A");
			a[i] = sc.nextInt();
		}
		for (int i = 0; i < b.length; i++) {
			System.out.println("Informe o elemento n�mero: " + (i + 1) + " do vetor B");
			b[i] = sc.nextInt();
		}
		for (int i = 0; i < r.length; i++) {
			r[i] = a[i] * b[i];
		}
		for (int i = 0; i < r.length; i++) {
			System.out.println("Posi��o " + (i + 1) + ":" + r[i]);
		}

	}

	public static void questao8() {

		String[] nome = new String[10];
		Double[] salario = new Double[10];

		for (int i = 0; i < salario.length; i++) {
			System.out.println("Informe o nome: ");
			nome[i] = sc.nextLine();

			System.out.println("Informe o sal�rio: ");
			salario[i] = sc.nextDouble();
			sc.nextLine();
		}

		for (int i = 0; i < salario.length; i++) {
			if (salario[i] < 600) {
				System.out.println(nome[i] + " est� isento, pois seu sal�rio de " + salario[i] + "� inferior a 600R$.");

			} else if (salario[i] >= 600 && salario[i] < 1500) {
				salario[i] = salario[i] * 0.90;
				System.out.println("nome[i]" + "Sal�rio de: " + salario[i]);
			} else {
				salario[i] = salario[i] * 0.85;
				System.out.println("nome[i]" + "Sal�rio de: " + salario[i]);
			}
		}
	}

	public static void questao9() {
		int a = 0;
		int cont = 0;
		System.out.println("Informe o n�mero");
		a = sc.nextInt();
		do {
			cont++;
			if (cont % 2 != 0) {
				a = a - cont;
			} else {
			}
		} while (a >= 0);
		System.out.println("A raiz �: " + (cont / 2));
	}

	public static void questao10() {
		int[] n = new int[10];
		int aux = 0;

		for (int i = 0; i < n.length; i++) {
			System.out.println("Informe o n�mero para a posi��o " + (i + 1) + " do vetor n: ");
			n[i] = sc.nextInt();
		}
		for (int i = 0; i < n.length; i++) {

			for (int j = 0; j < n.length; j++) {
				if (n[j] > n[i]) {
					aux = n[i];
					n[i] = n[j];
					n[j] = aux;
				}
			}
		}
		for (int i = 0; i < n.length; i++) {
			System.out.println((i + 1) + " posi��o: " + n[i]);
		}

	}

	public static void questao11() {
		int x = 0, r = 0;
		int[] vetor = new int[10];
		System.out.println("Informe o n�mero inicial: ");
		x = sc.nextInt();
		System.out.println("Informe a raz�o: ");
		r = sc.nextInt();

		for (int i = 0; i < vetor.length; i++) {
			vetor[i] = x + (i * r);
			System.out.println("PA: " + vetor[i]);

		}

	}

	public static void questao12() {
		int x = 0, r = 0;
		int[] vetor = new int[10];
		System.out.println("Informe o n�mero inicial: ");
		x = sc.nextInt();
		System.out.println("Informe a raz�o: ");
		r = sc.nextInt();

		for (int i = 0; i < vetor.length; i++) {
			vetor[i] = (int) (x * Math.pow(r, i));
			System.out.println("PG: " + vetor[i]);
		}

	}

	public static void questao13() {

		Random r = new Random();

		int numeroUsuario = 0;
		int[] numeroRandom = new int[10];

		for (int i = 0; i < numeroRandom.length; i++) {
			numeroRandom[i] = r.nextInt(10);
		}

		System.out.println("Informe o n�mero, usu�rio: ");
		numeroUsuario = sc.nextInt();

		for (int i = 0; i < numeroRandom.length; i++) {
			if (numeroRandom[i] == numeroUsuario) {

				System.out.println("Numero na posi��o " + i + " � igual ao " + numeroUsuario);
			}
		}

	}

	public static void questao14() {
		int[][] matriz = new int[4][4];
		int maior = 0;
		String var = null;
		Random r = new Random();
		for (int i = 0; i < matriz.length; i++) {
			for (int j = 0; j < matriz.length; j++) {
				matriz[i][j] = r.nextInt(10);
				System.out.print(matriz[i][j] + " ");
			}
			System.out.println("");
		}
		for (int i = 0; i < matriz.length; i++) {
			for (int j = 0; j < matriz.length; j++) {
				if (maior <= matriz[i][j]) {
					maior = matriz[i][j];
					var = "linha: " + i + "Coluna: " + j;
				}
			}
		}
		System.out.println("O maior elemento da matriz �: " + maior + ", o qual � situado na posi��o	" + var);
	}

	public static void questao15() {

		int[][] matriz = new int[10][10];
		int maior5 = 0, maior7 = 0, menor5 = 0, menor7 = 0;
		Random r = new Random();
		for (int i = 0; i < matriz.length; i++) {
			for (int j = 0; j < matriz.length; j++) {
				matriz[i][j] = r.nextInt(10);
				System.out.print(matriz[i][j] + " ");
			}
			System.out.println("");
		}
		for (int i = 5; i < 6; i++) {
			for (int j = 0; j < matriz.length; j++) {
				if (maior5 < matriz[i][j]) {
					maior5 = matriz[i][j];

				}
			}
		}
		for (int i = 7; i < 8; i++) {
			for (int j = 0; j < matriz.length; j++) {
				if (maior7 < matriz[i][j]) {
					maior7 = matriz[i][j];

				}
			}
		}
		for (int i = 5; i < 6; i++) {
			for (int j = 0; j < matriz.length; j++) {
				if (menor5 > matriz[i][j]) {
					menor5 = matriz[i][j];

				}
			}
		}
		for (int i = 7; i < 8; i++) {
			for (int j = 0; j < matriz.length; j++) {
				if (menor7 > matriz[i][j]) {
					menor7 = matriz[i][j];

				}
			}
		}
		System.out.println(" Maior n�mero da linha 5: " + maior5 + " \n Menor n�mero da linha 5: " + menor5
				+ "\n Maior n�mero da linha 7: " + maior7 + "\n Menor n�mero da linha 7: " + menor7);

	}

	public static void questao16() {

		int[][] matrizUsuario = new int[3][3];
		int contPar = 0, contImpar = 0;
		for (int i = 0; i < matrizUsuario.length; i++) {
			for (int j = 0; j < matrizUsuario.length; j++) {
				System.out.println("Por favor, informe a posi��o " + i + " " + j + " da matriz: ");
				matrizUsuario[i][j] = sc.nextInt();
			}
		}
		for (int i = 0; i < matrizUsuario.length; i++) {
			for (int j = 0; j < matrizUsuario.length; j++) {
				if (matrizUsuario[i][j] % 2 == 0) {
					contPar++;
				} else {
					contImpar++;
				}
			}
		}
		System.out.println("A quantidade de n�meros pares na matriz s�o: " + contPar + "\n e de �mpares: " + contImpar);

	}

	public static void questao17() {

		int[][] matrizA = new int[5][5];
		int[][] matrizB = new int[5][5];
		int[][] matrizC = new int[5][5];

		Random r = new Random();

		for (int i = 0; i < matrizA.length; i++) {
			for (int j = 0; j < matrizA.length; j++) {
				matrizA[i][j] = r.nextInt(5);
			}
		}

		for (int i = 0; i < matrizB.length; i++) {
			for (int j = 0; j < matrizB.length; j++) {
				matrizB[i][j] = r.nextInt(5);
			}
		}
		for (int i = 0; i < matrizC.length; i++) {
			for (int j = 0; j < matrizC.length; j++) {
				matrizC[i][j] = matrizA[i][j] + matrizB[i][j];
			}
		}

		for (int i = 0; i < matrizC.length; i++) {
			for (int j = 0; j < matrizC.length; j++) {
				System.out.print(matrizC[i][j] + " ");
			}
			System.out.println();
		}

	}

	public static void questao18() {

		int[] arrayA = new int[5];
		int[] arrayB = new int[8];
		int[] arrayC = new int[13];
		Random r = new Random();
		for (int i = 0; i < arrayA.length; i++) {
			arrayA[i] = r.nextInt(10);
		}
		for (int i = 0; i < arrayB.length; i++) {
			arrayB[i] = r.nextInt(10);
		}

		int a = 0;
		int b = 0;
		for (int i = 0; i < arrayC.length; i++) {
			if (i % 2 == 0 && a < arrayA.length) {
				arrayC[i] = arrayA[a];
				a++;
			} else {
				arrayC[i] = arrayB[b];
				b++;
			}

		}
		for (int i = 0; i < arrayC.length; i++) {
			System.out.println(arrayC[i]);
		}

	}

	public static void questao19() {

		int[] arrayElementos = new int[20];

		Random r = new Random();

		for (int i = 0; i < arrayElementos.length; i++) {
			arrayElementos[i] = r.nextInt(100);
			System.out.println("Elemento posi��o " + i + ": " + arrayElementos[i]);
		}

	}

	public static void questao20() {
		int novaNota = 0;
		int  aux = 0;
		int cont = 0;
		int [] notasEnem = new int [10];

		for (int i = 0; i < notasEnem.length; i++) {
			System.out.println("Informe a nota "+(i+1)+":");
			notasEnem[i] = sc.nextInt();
		}
		Arrays.sort(notasEnem);

		for (int i = notasEnem.length-1; i>=0; i--) {
			System.out.println(notasEnem[i]);
		}


		System.out.println("Cadastre uma nova nota: ");
		novaNota = sc.nextInt();
		for (int i = 0; i < notasEnem.length; i++) {
			if(novaNota>notasEnem[i] && cont==0){
				notasEnem[i] = novaNota;
				cont++;
			}
		}

		Arrays.sort(notasEnem);

		for (int i = notasEnem.length-1; i>=0; i--) {
			System.out.println(notasEnem[i]);
		}


	}

}
