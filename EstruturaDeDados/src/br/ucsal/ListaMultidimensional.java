package br.ucsal;

public class ListaMultidimensional {
	static NOC inicio = null;

	public static void main(String[] args) {
		inserir("eduardo");

		inserir("gabriel");

		inserir("danilo");

		inserirItem("eduardo", "20");

		inserirItem("gabriel", "primeiro");
		inserirItem("gabriel", "segundo");
		inserirItem("gabriel", "terceiro");
		inserirItem("gabriel", "quarto");
		removerItem("gabriel", "terceiro");
		removerCategoria("danilo");
		imprimirTudo();
	}

	public static void removerCategoria(String descricaoCategoria) {
		if (inicio == null) {
			System.out.println("Lista Vazia");
		} else {
			NOC categoria = inicio;
			NOC ant = null;
			while (categoria.prox != null && !categoria.descricao.equals(descricaoCategoria)) {
				ant = categoria;
				categoria = categoria.prox;
			}

			if (categoria.prox == null && categoria.descricao.equals(descricaoCategoria)) {
				ant.prox = null;
			} else if (categoria == inicio && categoria.descricao.equals(descricaoCategoria)) {
				inicio = inicio.prox;

			} else {
				if (categoria.descricao.equals(descricaoCategoria)) {
					ant.prox = categoria.prox;
				} else {
					System.out.println("Descricao invalida");
				}

			}

		}
	}

	public static void imprimirItens(String descricaoCategoria) {

		if (inicio == null)
			System.out.println("Lista vazia");
		else {

			NOC categoria = inicio;
			while (categoria.prox != null && !categoria.descricao.equals(descricaoCategoria)) {
				categoria = categoria.prox;
			}
			if (categoria.ini == null) {
				System.out.println("N�o h� itens");

			} else if (categoria.descricao.equals(descricaoCategoria)) {
				NOI item = categoria.ini;
				while (item != null) {
					System.out.println(item.descricao);
					item = item.prox;
				}
			}
		}
	}

	public static void removerItem(String descricaoCategoria, String descricaoItem) {

		if (inicio == null) {
			System.out.println("Lista principal vazia");
		} else {
			NOC categoria = buscar(descricaoCategoria);
			while (categoria != null && !categoria.descricao.equals(descricaoCategoria)) {
				categoria = categoria.prox;
			}
			if (categoria.descricao.equals(descricaoCategoria)) {
				if (categoria.ini == null) {
					System.out.println("N�o h� itens na categoria " + categoria.descricao);
				} else {
					NOI item = categoria.ini;
					NOI ant = null;
					// item.descricao = descricaoItem;
					while (item.prox != null && !item.descricao.equals(descricaoItem)) {
						ant = item;
						item = item.prox;

					}

					if (ant == null && item.descricao.equals(descricaoItem)) {
						categoria.ini = categoria.ini.prox;

					} else if (item.prox == null && item.descricao.equals(descricaoItem)) {

						ant.prox = null;
					} else {
						ant.prox = item.prox;
					}

				}

			}

		}

	}

	public static class NOC {

		public String descricao;
		public NOC prox;
		public NOI ini;

	}

	public static class NOI {

		public String descricao;
		public NOI prox;

	}

	public static void inserir(String descricao) {

		NOC novo = new NOC();
		novo.descricao = descricao;
		if (inicio == null)
			inicio = novo;
		else {
			NOC aux = inicio;
			while (aux.prox != null) {
				aux = aux.prox;

			}
			aux.prox = novo;

		}
	}

	public static void imprimirTudo() {

		if (inicio == null)
			System.out.println("Lista vazia!");
		else {
			NOC aux = inicio;
			while (aux != null) {
				System.out.println("Categoria: " + aux.descricao);

				NOI auxDois = aux.ini;
				while (auxDois != null) {
					System.out.println("Item: " + " " + auxDois.descricao + " ");

					auxDois = auxDois.prox;

				}
				aux = aux.prox;
			}
		}
	}

	public static void imprimirLista() {
		if (inicio == null)
			System.out.println("Lista vazia!");
		else {
			NOC aux = inicio;
			while (aux != null) {
				System.out.println(aux.descricao);
				aux = aux.prox;
			}
		}

	}

	public static NOC buscar(String descricao) {
		if (inicio == null)
			return null;
		else {
			NOC aux = inicio;
			NOC ant = null;
			while (aux.prox != null && !aux.descricao.equals(descricao)) {
				ant = aux;
				aux = aux.prox;
			}
			if (aux.descricao.equals(descricao))
				return aux;
			else {
				return null;
			}
		}
	}

	public static void inserirItem(String descricaoCategoria, String descricaoItem) {
		if (inicio == null) {
			System.out.println("Lista Vazia");
		} else {
			NOC categoria = buscar(descricaoCategoria);
			if (categoria == null) {
				System.out.println("Categoria inexistente!");
			} else {
				NOI novo = new NOI();
				novo.descricao = descricaoItem;

				if (categoria.ini == null) {
					categoria.ini = novo;
				} else {
					novo.prox = categoria.ini;
					categoria.ini = novo;
				}

			}
		}

	}

}
